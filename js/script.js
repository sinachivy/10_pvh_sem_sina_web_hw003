
let startTimeTxt = document.getElementById('startTimeTxt')
let stopTimeTxt = document.getElementById('stopTimeTxt')
let totalTimeTxt = document.getElementById('totalTimeTxt')
let rielTxt = document.getElementById('rielTxt')
let localTimeTxt = document.getElementById('localTimeTxt')

let btnStart = document.getElementById('btnStart')
let btnEnd = document.getElementById('btnEnd')

let datatime = new Date()

//local time at buttom
setInterval(function () {
  let datetime = new Date();
  localTimeTxt.innerText = datetime.toDateString() + " | " + datetime.toLocaleTimeString()
}, 1000)


startbtn.addEventListener('click', () => {
  start()
  startbtn.style.display = 'none'
  endbtn.style.display = 'block'
})

endbtn.addEventListener('click', () => {
  end()
 endbtn.style.display = 'none'
  clearbtn.style.display = 'block'
})

clearbtn.addEventListener('click', () => {
    startTimeTxt.innerText = `00 : 00`
    stopTimeTxt.innerText = `00 : 00`
    totalTimeTxt.innerText = 'minute (s)'
    rielTxt.innerText = '0'

    clearbtn.style.display = 'none'
    startbtn.style.display = 'block'
})

let startDate
let timeStart

let endDate
let timeEnd


let start = () => {
  startDate = new Date();
  timeStart = startDate.toLocaleTimeString();
  startTimeTxt.innerText = timeStart
}
let end = () => {
  endDate = new Date();
  timeEnd = endDate.toLocaleTimeString();

  stopTimeTxt.innerText = timeEnd

  let totalMinute = ((endDate.getTime() - startDate.getTime()) / 1000) / 60;
  // let totalMinute = 45
  let HourFirst = parseInt(totalMinute / 60)
  let MinuteLast = totalMinute % 60 
  totalTimeTxt.innerText = totalMinute.toFixed() + " minute(s)";

  let totalmoney;

  if(MinuteLast < 15){
    totalmoney = 500;
  }
  else if(MinuteLast <= 30 && MinuteLast > 15){
    totalmoney = 1000
  }
  else if(MinuteLast <= 60 && MinuteLast > 30){
    totalmoney = 1500
  }
  
  totalmoney = totalmoney + HourFirst * 1500;
  
  rielTxt.innerText = totalmoney
}